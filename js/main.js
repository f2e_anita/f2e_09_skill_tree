$(window).on("load", function() {
  var cvs = document.querySelector("#cvs");
  var ctx = cvs.getContext("2d");

  ctx.strokeStyle = "#FF0093";
  ctx.beginPath();
  ctx.moveTo(0, 30);
  ctx.lineTo(0, 150);
  ctx.lineTo(300, 150);
  ctx.lineTo(300, 30);
  ctx.lineTo(100, 30);
  ctx.lineTo(113, 23);
  ctx.lineTo(103, 18);
  ctx.lineTo(140, 0);
  ctx.lineTo(75, 14);
  ctx.lineTo(85, 20);
  ctx.lineTo(60, 30);
  ctx.lineTo(0, 30);
  ctx.closePath();
  ctx.stroke();
});
